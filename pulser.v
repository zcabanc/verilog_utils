module pulser (
  clk, trigger, pulseout
);

input clk, trigger;
reg [31:0] counter = 1;
reg [31:0] triggered_at = 0;
output reg pulseout = 0;

always @(posedge clk) begin
  counter <= counter + 1;
  if (triggered_at == counter) begin
    pulseout <= 1;
  end else begin
    pulseout <= 0;
  end
end

always @(posedge trigger) begin
  triggered_at <= counter + 1;
end

endmodule
